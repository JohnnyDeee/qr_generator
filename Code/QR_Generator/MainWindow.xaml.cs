﻿using Gma.QrCodeNet.Encoding;
using Gma.QrCodeNet.Encoding.Windows.Render;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using MahApps.Metro.Controls;

namespace QR_Generator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        List<System.Windows.Controls.TextBox> textRows = new List<System.Windows.Controls.TextBox>();

        public MainWindow()
        {
            InitializeComponent();
        }

        // Save to image file
        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            // Disable the button
            object buttonContent_cache = button_save.Content;
            button_save.IsEnabled = false;
            button_save.Content = "Working...";

            BitMatrix qrMatrix = qrControl.GetQrMatrix();

            // Path validation
            string path = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // My documents folder
            if (text_savetofolder.Text != "")
            {
                if (Directory.Exists(text_savetofolder.Text))
                    path = text_savetofolder.Text;
                else
                {
                    System.Windows.MessageBox.Show("Make sure the provided path is valid.", "error", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            // Filename validation
            string filename = text_filename.Text != "" ? text_filename.Text : "qrcode";

            // Setup Image format
            ImageFormatEnum extension;
            string str_extension = (combo_extension.SelectedValue as ComboBoxItem).Content.ToString().Replace(".", "").ToUpper();
            Enum.TryParse<ImageFormatEnum>(str_extension, out extension);
            Console.WriteLine(str_extension);

            // Setup path
            path = System.IO.Path.Combine(path, filename);

            // Choose for bulk or single generation
            if (!check_multigenerations.IsChecked.Value)
            {
                SaveCodeToDrive(qrMatrix, path + "." + extension.ToString().ToLower(), extension);
                
                // Show the file when done
                Process.Start("explorer.exe", "/select," + path + "." + extension.ToString().ToLower());
            }
            else
            {
                int index = 1;
                foreach (System.Windows.Controls.TextBox textbox in textRows)
                {
                    qrControl.Text = textbox.Text;
                    if (qrControl.Text != "")
                    {
                        qrMatrix = qrControl.GetQrMatrix();
                        SaveCodeToDrive(qrMatrix, path + "_" + index + "." + extension.ToString().ToLower(), extension);
                    }

                    index++;
                }

                // Show the file when done
                Process.Start("explorer.exe", "/select," + path + "_" + (index-1) + "." + extension.ToString().ToLower());
            }

            // Enable the button
            button_save.IsEnabled = true;
            button_save.Content = buttonContent_cache;
        }

        // Start generation
        private void text_qrtext_TextChanged(object sender, TextChangedEventArgs e)
        {
            string textToEncode = text_qrtext.Text;
            qrControl.Text = textToEncode;
        }

        // Open folder dialog
        private void button_savetofolder_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog folderDiag = new FolderBrowserDialog();
            folderDiag.Description = "Select a folder where you want to save the qr image to";
            DialogResult result = folderDiag.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                text_savetofolder.Text = folderDiag.SelectedPath;
            }
        }

        // Enable bulk generation
        private void check_multigenerations_Click(object sender, RoutedEventArgs e)
        {
            if (check_multigenerations.IsChecked.Value)
            {
                text_multigenerations.IsEnabled = true;
                exp_textList.IsEnabled = true;
                text_qrtext.IsEnabled = false;
                TextBoxHelper.SetWatermark(text_qrtext, "Use the Text List below...");
                TextBoxHelper.SetIsWaitingForData(text_qrtext, false);
                button_save.Content = "Start";
            }
            else
            {
                text_multigenerations.IsEnabled = false;
                exp_textList.IsEnabled = false;
                exp_textList.IsExpanded = false;
                text_qrtext.IsEnabled = true;
                TextBoxHelper.SetWatermark(text_qrtext, "Enter text here...");
                TextBoxHelper.SetIsWaitingForData(text_qrtext, true);
                button_save.Content = "Save";
            }
        }

        // Filter input
        private void text_multigenerations_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            // Only allow numbers
            Regex regx = new Regex(@"^\d+$");
            if (regx.IsMatch(e.Text))
                e.Handled = false;
            else
                e.Handled = true;
        }

        // Show TextList expander
        private void exp_options_Collapsed(object sender, RoutedEventArgs e)
        {
            exp_textList.Visibility = Visibility.Visible;

            // Clear stack panel childrens
            StackPanel stack_panel = (this.FindName("stack_textlist") as StackPanel);
            stack_panel.Children.Clear();
            textRows.Clear();

            // Check if generations number is set
            if (text_multigenerations.Text == "")
            {
                // Add label child to stack panel
                // indicating generations number is not set
                System.Windows.Controls.Label label = new System.Windows.Controls.Label();
                TextBlock textBlock = new TextBlock();
                textBlock.Text = "Please specify a number in the options\nfor the amount of generations...";
                textBlock.TextAlignment = TextAlignment.Center;
                textBlock.Foreground = Brushes.Gray;
                //textBlock.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                label.Content = textBlock;
                label.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
                stack_panel.Children.Add(label);
                return;
            }            

            // Add children to stack panel
            int generations = Int32.Parse(text_multigenerations.Text);
            for (int i = 1; i <= generations; i++)
            {
                System.Windows.Controls.TextBox row = new System.Windows.Controls.TextBox();
                row.Height = 23;
                row.Width = 320;
                row.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                row.TextWrapping = TextWrapping.NoWrap;
                row.Name = "text_row" + i;
                TextBoxHelper.SetWatermark(row, "Enter text here...");

                stack_panel.Children.Add(row);
                textRows.Add(row);
            }
        }

        // Hide TextList expander
        private void exp_options_Expanded(object sender, RoutedEventArgs e)
        {
            exp_textList.Visibility = Visibility.Hidden;
        }

        // Save QR code to image file
        private void SaveCodeToDrive(BitMatrix qrMatrix, string path, ImageFormatEnum imageFormat, bool openWhenDone = false)
        {
            // Write matrix to a renderer
            // then to a filestream
            DrawingBrushRenderer dRenderer = new DrawingBrushRenderer(new FixedModuleSize(20, QuietZoneModules.Two), Brushes.Black, Brushes.White); // FixedModuleSize(2, ..) = 50x50 pixels
            using (FileStream fStream = File.Create(path))
            {
                dRenderer.WriteToStream(qrMatrix, imageFormat, fStream, new Point(96, 96));
            }
        }
    }
}
